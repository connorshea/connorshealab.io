module TagPlugin
  class TagPageGenerator < Jekyll::Generator
    safe true

    def generate(site)
      site.data.dig('tags').each do |_name, tag|
        site.pages << TagPage.new(site, tag)
      end
    end
  end

  # Subclass of `Jekyll::Page` with custom method definitions.
  class TagPage < Jekyll::Page
    def initialize(site, tag)
      @site = site
      @base = site.source
      @dir  = "#{tag['slug']}/"

      @basename = 'index'
      @ext      = '.html'
      @name     = 'index.html'

      self.process(name)
      @data = {
        'layout' => 'tag',
        'title' => "Posts tagged #{tag['name']}",
        'tagged_posts' => site.tags.dig(tag['slug']),
        'tag' => tag
      }

      # Magic that makes the permalink to `/blog/tags/foo/index.html` work.
      # Not entirely sure how this works honestly.
      data.default_proc = proc do |_, key|
        site.frontmatter_defaults.find(relative_path, :tags, key)
      end
    end

    # Placeholders that are used in constructing page URL.
    def url_placeholders
      {
        tag: @dir,
        basename: basename,
        output_ext: output_ext
      }
    end
  end
end
